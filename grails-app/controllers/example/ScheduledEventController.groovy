package example

import au.com.bytecode.opencsv.CSVWriter
import grails.transaction.Transactional

import static org.springframework.http.HttpStatus.*

@Transactional(readOnly = true)
class ScheduledEventController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def grailsApplication  //inject GrailsApplication

    def downloadReport() {
        response.setHeader("Content-disposition", "attachment; filename=events.csv")
        response.contentType = "text/csv"
        def out = response.outputStream
        out.withWriter { writer ->

            String[] properties = new String[9]
            def csvWriter = new CSVWriter(writer)

            // create the headings
            properties[0] = "Title"
            properties[1] = "Description"
            properties[2] = "Active"
            properties[3] = "Number of Speakers"
            properties[4] = "Admission Price"
            properties[5] = "Event Date"
            properties[6] = "Updated"
            properties[7] = "Created"
            properties[8] = "Event Type"
            csvWriter.writeNext(properties)
            // fill in data
            ScheduledEvent.findAll().each { scheduledEvent ->
                properties[0] = scheduledEvent.title
                properties[1] = scheduledEvent.description
                properties[2] = scheduledEvent.active
                properties[3] = scheduledEvent.numberOfSpeakers
                properties[4] = scheduledEvent.admissionPrice
                properties[5] = scheduledEvent.eventDate
                properties[6] = scheduledEvent.lastUpdated
                properties[7] = scheduledEvent.dateCreated
                properties[8] = scheduledEvent.eventType
                csvWriter.writeNext(properties)
            }
            csvWriter.flush()
        }
    }


    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond ScheduledEvent.list(params), model: [scheduledEventInstanceCount: ScheduledEvent.count()]
    }

    def show(ScheduledEvent scheduledEventInstance) {
        respond scheduledEventInstance
    }

    def create() {
        respond new ScheduledEvent(params)
    }

    @Transactional
    def save(ScheduledEvent scheduledEventInstance) {
        if (scheduledEventInstance == null) {
            notFound()
            return
        }

        if (scheduledEventInstance.hasErrors()) {
            respond scheduledEventInstance.errors, view: 'create'
            return
        }

        scheduledEventInstance.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'domain.scheduledEvent', default: 'ScheduledEvent'), scheduledEventInstance.id])
                redirect scheduledEventInstance
            }
            '*' { respond scheduledEventInstance, [status: CREATED] }
        }
    }

    def edit(ScheduledEvent scheduledEventInstance) {
        respond scheduledEventInstance
    }

    @Transactional
    def update(ScheduledEvent scheduledEventInstance) {
        if (scheduledEventInstance == null) {
            notFound()
            return
        }

        if (scheduledEventInstance.hasErrors()) {
            respond scheduledEventInstance.errors, view: 'edit'
            return
        }

        scheduledEventInstance.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'domain.scheduledEvent', default: 'ScheduledEvent'), scheduledEventInstance.id])
                redirect scheduledEventInstance
            }
            '*' { respond scheduledEventInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(ScheduledEvent scheduledEventInstance) {

        if (scheduledEventInstance == null) {
            notFound()
            return
        }

        scheduledEventInstance.delete flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'domain.scheduledEvent', default: 'ScheduledEvent'), scheduledEventInstance.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'domain.scheduledEvent', default: 'ScheduledEvent'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}

