import example.EventType
import example.ScheduledEvent
import example.security.*

class BootStrap {

    def init = { servletContext ->
        def today = new Date()

        //Security
        def userRole = SecRole.findByAuthority('ROLE_USER') ?: new SecRole(authority: 'ROLE_USER').save(failOnError: true)
        def adminRole = SecRole.findByAuthority('ROLE_ADMIN') ?: new SecRole(authority: 'ROLE_ADMIN').save(failOnError: true)
        def adminUser = SecUser.findByUsername('admin') ?: new SecUser(
                username: 'admin',
                password: 'admin',
                enabled: true).save(failOnError: true)

        def basicUser = SecUser.findByUsername('guest') ?: new SecUser(
                username: 'guest',
                password: 'guest',                          //pw encoded by security plugin
                enabled: true).save(failOnError: true)


        if (!adminUser.authorities.contains(adminRole)) {
            SecUserSecRole.create adminUser, adminRole
        }
        if (!basicUser.authorities.contains(userRole)) {
            SecUserSecRole.create basicUser, userRole
        }


        Calendar cal = Calendar.getInstance();
        cal.setTime(today);
        cal.add(Calendar.DATE, -1);
        Date yesterday = cal.getTime();

        def eventType1 = new EventType(name: 'Ted Talk', description: 'TED is a nonprofit devoted to Ideas Worth Spreading - through TED.com, our annual conferences, the annual TED Prize and local TEDx events.').save(failOnError: true)
        def eventType2 = new EventType(name: 'No Fuff', description: 'No Fluff, Just Stuff Java Symposium A Java symposium series in 25 cities throughout the U.S. and Canada.').save(failOnError: true)

        def event1 = new ScheduledEvent(title: 'Senior Days at the Bargain Box', description: "Senior Days are BACK!! All shoppers, 60 years old or older, will receive 20% off every Wednesday!!", eventDate: today, admissionPrice: 4.00, numberOfSpeakers: 2, active: true, eventType: eventType1).save(failOnError: true)
        def event2 = new ScheduledEvent(title: 'A Juneteenth Celebration', description: "Join the International Civil Rights Center & Museum for “ A Juneteenth Celebration", eventDate: today, admissionPrice: 1900.99, numberOfSpeakers: 70, active: true, eventType: eventType2).save(failOnError: true)
        def event3 = new ScheduledEvent(title: 'Dance Project the School', description: "Summer camps available for ages 3-18; technique classes for 18 mo.-100; drop-in classes available for 13+ Modern, Ballet, Jazz, Creative Movement, Musical Theatre, Tap .", eventDate: today, admissionPrice: 350.00, numberOfSpeakers: 15, active: true, eventType: eventType1).save(failOnError: true)
        def event4 = new ScheduledEvent(title: 'Greenhill Exhibition | Jonathan Brilliant On-Site ', description: "Jonathan Brilliant On-Site, on display at Greenhill June 19 – August 30, is a one-person exhibition by Raleigh-based artist Jonathan Brilliant who will create an evolving sculptural environment from...", eventDate: today, admissionPrice: 199.99, numberOfSpeakers: 7, active: true, eventType: eventType2).save(failOnError: true)
    }
    def destroy = {
    }
}
