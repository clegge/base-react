<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title><g:layoutTitle default="Grails"/></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="${assetPath(src: 'favicon.ico')}" type="image/x-icon">
    <link rel="apple-touch-icon" href="${assetPath(src: 'apple-touch-icon.png')}">
    <link rel="apple-touch-icon" sizes="114x114" href="${assetPath(src: 'apple-touch-icon-retina.png')}">
    <script src="http://fb.me/JSXTransformer-0.13.3.js"></script>
    <asset:stylesheet src="application.css"/>
    <asset:javascript src="application.js"/>
    <g:layoutHead/>
</head>

<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="haecoLogo" href="${createLink(uri: '/')}"><g:img dir="images" file="haecoLogo.png" height="30"/></a>
        </div>
        <ul class="nav navbar-nav">
            <li><g:link controller="ScheduledEvent"><g:message code="scheduledEvents" /></g:link></li>
            <sec:ifAnyGranted roles="ROLE_ADMIN">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Admin <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><g:link controller="ScheduledEvent">Customer</g:link></li>
                        %{--                        <li class="divider"></li>
                                                <li class="dropdown-header">Security:</li>
                                                <li><g:link controller="ScheduledEvent">Role</g:link></li>
                                                <li><g:link controller="ScheduledEvent">User</g:link></li>
                                                <li><g:link controller="ScheduledEvent">User/Role Assignment</g:link></li>--}%
                    </ul>
                </li>
            </sec:ifAnyGranted>
        </ul>
        <div id="navbar" class="navbar-collapse collapse">
            <sec:ifNotLoggedIn>
            <form class="navbar-form navbar-right"  method="POST"action="${resource(file: 'j_spring_security_check')}">
                <div class="form-group">
                    <input type="text" placeholder="Username" class="form-control" name='j_username' id='username'/>
                </div>

                <div class="form-group">
                    <input type="password" placeholder="Password" class="form-control" name='j_password' id='password'/>
                </div>
                <button type="submit" class="btn btn-success">Sign in</button>
            </form>
            </sec:ifNotLoggedIn>
            <sec:ifLoggedIn>
                <ul class="nav navbar-nav navbar-right">
                    <li><g:link controller="logout">[ Log Out ]</g:link></li>
                </ul>
            </sec:ifLoggedIn>
        </div><!--/.navbar-collapse -->
    </div>
</nav>
<g:layoutBody/>
<div class="footer" role="contentinfo">
    &copy; 2014 HAECO Americas
</div>
</body>
</html>
