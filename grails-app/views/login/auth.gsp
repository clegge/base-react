<html>
<head>
    <meta name="layout" content="main"/>
    <title><g:message code="springSecurity.login.title"/></title>
</head>

<body>
<form action="${postUrl}" method="POST" id="loginForm" class="form-horizontal" autocomplete="off">



    <div class="container wrapperAuth">
        <div class="col-xs-4"></div>

        <div class="col-xs-4 bordered">
            <div class="row col-xs-12 authFormMarginTop ">
                <h1><g:message code="security.login.header"/></h1>

                <g:if test="${flash.message}">
                    <div class="login_message">${flash.message}</div>
                </g:if>
            </div>

            <div class="row required form-group">
                <label for="username" class="col-xs-5 control-label">
                    <g:message code="security.login.username"/>
                </label>

                <div class="col-xs-7">
                    <input type="text" class="form-control" name="j_username" id="username"/>
                </div>
            </div>


            <div class="row required form-group">
                <label for="password" class="col-xs-5 control-label ">
                    <g:message code="security.login.password"/>
                </label>

                <div class="col-xs-7">
                    <input type="password" class="form-control" name="j_password" id="password"/>
                </div>
            </div>

            <div class="row form-group">

                <label for="remember_me" class="col-xs-5 control-label ">
                    <g:message code="security.login.rememberMe"/>
                </label>

                <div class="col-xs-7">
                    <input type="checkbox" class="form-control-checkbox" name="${rememberMeParameter}" id="remember_me" <g:if test="${hasCookie}">checked="checked"</g:if>/>
                </div>

            </div>

            <div class="row marginTopDouble form-group">
                <div class="col-xs-12">
                    <button name="login" type="submit" class=" btn btn-success btn-block " aria-label="Left Align">
                        <span class="fa fa-shield fa-lg" aria-hidden="true"> Login</span>
                    </button>
                </div>
            </div>


            <div class="col-xs-4"></div>
        </div>
</form>

</body>
</html>
