<%@ page import="example.ScheduledEvent" %>
<%@ page import="example.EventType" %>
<hr>

<div class="form-group">
    <label for="title" class="col-xs-3 control-label">
        <span class="glyphicon glyphicon-asterisk"/></span><g:message code="scheduledEvent.label.title"/>
    </label>

    <div class="col-xs-9">
        <formHelper:getFocus name="title" class="form-control" value="${scheduledEventInstance?.title}" required=""
                             actionValue="${actionName}"/>
    </div>
</div>

<div class="form-group">
    <label for="title" class="col-xs-3 control-label ">
        <span class="glyphicon glyphicon-asterisk"/></span><g:message code="scheduledEvent.label.active"/>
    </label>

    <div class="col-xs-9 ">
        <g:checkBox name="active" class="form-control-checkbox" value="${scheduledEventInstance?.active}"/>
    </div>
</div>

<div class="form-group">
    <label for="eventType" class="col-xs-3 control-label">
        <span class="glyphicon glyphicon-asterisk"/></span><g:message code="scheduledEvent.label.eventType"/>
    </label>

    <div class="col-xs-9">
        <g:select class="form-control" id="eventType" name='eventType' value="${scheduledEventInstance?.eventType?.id}"
                  noSelection="${['null': 'Select One...']}"
                  from='${EventType?.findAll()}'
                  optionKey="id" optionValue="name" required="">
        </g:select>
    </div>
</div>

<div class="form-group">
    <label for="title" class="col-xs-3 control-label">
        <span class="glyphicon glyphicon-asterisk"/></span><g:message code="scheduledEvent.label.numberOfSpeakers"/>
    </label>

    <div class="col-xs-9">

        <g:field class="form-control" type="number" min="1" max="1000000" step="0.0001" name="numberOfSpeakers"
                 value="${scheduledEventInstance?.numberOfSpeakers}" required=""/>
    </div>
</div>

<div class="form-group">
    <label for="title" class="col-xs-3 control-label">
        <span class="glyphicon glyphicon-asterisk"/></span><g:message code="scheduledEvent.label.admissionPrice"/>
    </label>

    <div class="col-xs-9">
        <g:field type="number" min="0" max="1000000" step="0.0001" name="admissionPrice" class="form-control"
                 value="${formatNumber(number: scheduledEventInstance.admissionPrice, g.message(code: 'default.number.format'))}"/>
    </div>
</div>

<div class="form-group">
    <label for="title" class="col-xs-3 control-label">
        <span class="glyphicon glyphicon-asterisk"/></span><g:message code="scheduledEvent.label.eventDate"
                                                                      default="Event Date"/>
    </label>

    <div class="col-xs-9">
        <g:textField class="form-control dateTimePicker" name="eventDate"
                     value="${formatDate(format: 'MM/dd/yyyy', date: scheduledEventInstance?.eventDate)}" required=""/>
    </div>

</div>

<div class="form-group">
    <label for="title" class="col-xs-3 control-label">
        <span class="glyphicon glyphicon-asterisk"/></span><g:message code="scheduledEvent.label.description"/>
    </label>

    <div class="col-xs-9">
        <textarea name="description" class="form-control" rows="5"
                  required="">${scheduledEventInstance?.description}</textarea>
    </div>
    <hr>
</div>


