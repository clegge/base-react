<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'domain.scheduledEvent', default: 'Scheduled Event')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<div class="container">
    <div class="row grailsDomainNav">
        <div class="col-xs-12">
            <ul>
                <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]"/></g:link></li>
            </ul>
        </div>
    </div>


    <div class="row">
        <div class="col-xs-12">
            <h1><g:message code="default.create.label" args="[entityName]"/></h1>

            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum nisi sapien, pellentesque sit amet tempus in, ultrices at tellus. Curabitur ut lacus a libero accumsan aliquet. Phasellus molestie rutrum diam, in pulvinar arcu blandit in.</p>
            <g:if test="${flash.message}">
                <div class="message" role="status">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${scheduledEventInstance}">
                <ul class="errors" role="alert">
                    <g:eachError bean="${scheduledEventInstance}" var="error">
                        <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                                error="${error}"/></li>
                    </g:eachError>
                </ul>
            </g:hasErrors>
        </div>
    </div>


    <sec:ifAnyGranted roles="ROLE_ADMIN">

        <g:form class="form-horizontal formNoTopPadOrMargin" url="[resource: scheduledEventInstance, action: 'save']">
            <fieldset class="form">
                <g:render template="form"/>
            </fieldset>
            <fieldset class="buttons">
                <button name="create" class="btn btn-success buttonWithForm" name="create">
                    <span class="fa fa-save fa-lg" aria-hidden="true">Create</span>
                </button>
            </fieldset>
        </g:form>

    </sec:ifAnyGranted>

</body>
</html>
