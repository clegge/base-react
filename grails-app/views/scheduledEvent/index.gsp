<%@ page import="example.ScheduledEvent" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'domain.scheduledEvent', default: 'Scheduled Event')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<div class="container">
    <div class="row grailsDomainNav">
        <div class="col-xs-12">
            <ul>
                <sec:ifAnyGranted roles="ROLE_ADMIN">
                    <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]"/></g:link></li>
                </sec:ifAnyGranted>
            </ul>
        </div>
    </div>


    <div class="row">
        <div class="col-xs-12">
            <h1><g:message code="default.list.label" args="[entityName]"/></h1>

            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum nisi sapien, pellentesque sit amet tempus in, ultrices at tellus. Curabitur ut lacus a libero accumsan aliquet. Phasellus molestie rutrum diam, in pulvinar arcu blandit in.</p>
            <g:if test="${flash.message}">
                <div class="message" role="status">${flash.message}</div>
            </g:if>
        </div>
    </div>


    <table class="table table-bordered table-striped dataTable" id="defaultDataTable">
        <thead>
        <tr>
            <th class="sortableHeader"><g:message code="generic.th.title"/></th>
            <th class="sortableHeader"><g:message code="generic.th.description"/></th>
            <th class="sortableHeader"><g:message code="generic.th.date"/></th>
        </tr>
        </thead>
        <tbody>
        <g:each in="${scheduledEventInstanceList}" status="i" var="scheduledEventInstance">
            <tr class="hover"
                onclick='document.location = "<g:createLink action='show' id="${scheduledEventInstance.id}"/>" '>
                <td>${fieldValue(bean: scheduledEventInstance, field: "title")}</td>
                <td>${fieldValue(bean: scheduledEventInstance, field: "description")}</td>
                <td class="text-nowrap"><g:formatDate format="MM/dd/yyyy"
                                                      date="${scheduledEventInstance.eventDate}"/></td>
            </tr>
        </g:each>
        </tbody>
    </table>

    <div class="row">
        <g:form controller="scheduledEvent" action="downloadReport">
            <button name="download" type="submit" class=" btn btn-success btn-block buttonWithForm"
                    aria-label="Left Align" action="downloadReport">
                <span class="fa fa-download fa-lg" aria-hidden="true">Export</span>
            </button>
        </g:form>
    </div>
</div>

</body>
</html>
