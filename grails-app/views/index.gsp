<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Welcome to Grails</title>

</head>

<body>

<div class="container">

    <div class="row">
        <h1>List Builder</h1>
        <div class="col-xs-12" id="react-list-builder">
        </div>
    </div>
    <hr>

%{--    <div class="row">
        <h1>List Box</h1>
        <div class="col-xs-12" id="tutorial">
        </div>
    </div>
    <hr>

    <div class="row">
        <h1>Friends Container</h1>
        <div class="col-xs-12" id="friendsContainer">
        </div>
    </div>
    <hr>
--}%
</div>
<script src="/base-react/assets/react/baseReact.js"></script>
</body>
</html>
