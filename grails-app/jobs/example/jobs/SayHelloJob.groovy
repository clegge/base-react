package example.jobs

import org.apache.log4j.Logger


class SayHelloJob {
    Logger logger = Logger.getLogger(SayHelloJob.class)
    def examplFormat = 'HH:mm:ss'



    static triggers = {
        cron name: 'myTrigger', cronExpression: "0 0/5 * * * ?"
    }

    def execute() {
        def now = new Date().format(examplFormat)
        logger.info("Hello from quartz, the time is $now... Turn me off in production!!!")
    }
}
