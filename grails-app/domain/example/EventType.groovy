package example

class EventType {
    Long id
    String name
    String description
    Date lastUpdated
    Date dateCreated

    static mapping = {
        description type: 'text'
    }

    static constraints = {
    }

    @Override
    String toString() {
        return name
    }
}
