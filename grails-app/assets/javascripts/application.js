// This is a manifest file that'll be compiled into application.js.
//
// Any JavaScript file within this directory can be referenced here using a relative path.
//
// You're free to add application-wide JavaScript to this file, but it's generally better 
// to create separate JavaScript files as needed.
//
//= require_tree libs
//= require_self

if (typeof jQuery !== 'undefined') {
    (function($) {
        $('#spinner').ajaxStart(function() {
            $(this).fadeIn();
        }).ajaxStop(function() {
            $(this).fadeOut();
        });
    })(jQuery);
}


$(document).ready(function() {
    $( ".dateTimePicker" ).datepicker();
});

$( document ).ready(function() {
    // bind DataTable on table ID
    $('#defaultDataTable').dataTable( {
        /* to increase the process and speed for loading data from server
         * (check more properties if needed )
         */
        "bProcessing": true,
        "sPaginationType": "full_numbers",
        "bPaginate": false,
        "bDeferRender": true
    });

    $('#aircraftTable').dataTable( {
        "bProcessing": true,
        "sPaginationType": "full_numbers",
        "bPaginate": false,
        "bDeferRender": true,
        "aoColumnDefs": [
            {
                "bSortable": false, "aTargets": [4,5],
                "bSearchable": false, "aTargets": [4,5]
            }
        ] } );

    //bindDataTable();
    chosen();
    bindDateTimePicker();
});


/* Chosen (select box - chosen) */
function chosen(){
    if($('.chosen-select').length > 0){
        $('.chosen-select').each(function(){
            var $el = $(this);
            var search = ($el.attr("data-nosearch") === "true") ? true : false,
                opt = {};
            if(search) opt.disable_search_threshold = 9999999;
            $el.chosen(opt);
        });
    }
}
function bindDateTimePicker(){
    /* date picker */
    if($('.datepick').length > 0){
        $('.datepick').datepicker();
    }
    /* date range picker */
    if($('.daterangepick').length > 0){
        $('.daterangepick').daterangepicker();
    }
    /* time picker */
    if($('.timepick').length > 0){
        $('.timepick').timepicker({
            defaultTime: 'current',
            minuteStep: 1,
            disableFocus: true,
            template: 'dropdown'
        });
    }
}

/* Chosen (select box - chosen) */
function chosen(){
    if($('.chosen-select').length > 0){
        $('.chosen-select').each(function(){
            var $el = $(this);
            var search = ($el.attr("data-nosearch") === "true") ? true : false,
                opt = {};
            if(search) opt.disable_search_threshold = 9999999;
            $el.chosen(opt);
        });
    }
}
function bindDateTimePicker(){
    /* date picker */
    if($('.datepick').length > 0){
        $('.datepick').datepicker();
    }
    /* date range picker */
    if($('.daterangepick').length > 0){
        $('.daterangepick').daterangepicker();
    }
    /* time picker */
    if($('.timepick').length > 0){
        $('.timepick').timepicker({
            defaultTime: 'current',
            minuteStep: 1,
            disableFocus: true,
            template: 'dropdown'
        });
    }
}
