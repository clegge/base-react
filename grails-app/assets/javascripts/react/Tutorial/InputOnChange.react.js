var React = require('react');


var HelloWorld = React.createClass({

    getInitialState: function () {
        return {
            userName: 'Clegge'
        }
    },

    handleChange: function (e) {
        this.setState({
            userName: e.target.value
        })
    },

    render: function () {
        return (
            <div>
                Hello {this.state.userName}! <br />
                How is {this.props.gfName} doing?<br />
                Change Name: <input type="text" value={this.state.userName} onChange={this.handleChange}/>
            </div>
        )
    }
});

var InputOnChange = React.createClass({
    render: function () {
        return (
            <HelloWorld gfName="Esperanza Legge" />
        )
    }
});

module.exports = InputOnChange;