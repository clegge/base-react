var React = require('react');


var FriendsContainer = React.createClass({
    getInitialState: function () {
        //alert('In getInitialState');
        return {
            name: 'Tyler McGinnis',
            friends: ['Jake Lingwall', 'Murphy Randall', 'Merrick Christensen']
        }
    },

    // Invoked once before first render
    componentWillMount: function(){
        // Calling setState here does not cause a re-render
        //alert('In Component Will Mount');
    },


    // Invoked once after the first render
    componentDidMount: function(){
        // You now have access to this.getDOMNode()
        //alert('In Component Did Mount');
    },

    // Invoked whenever there is a prop change
    // Called BEFORE render
    componentWillReceiveProps: function(nextProps){
        // Not called for the initial render
        // Previous props can be accessed by this.props
        // Calling setState here does not trigger an additional re-render
        //alert('In Component Will Receive Props');
    },


    // Called IMMEDIATELY before a component is unmounted
    componentWillUnmount: function(){},
    addFriend: function(friend){
        this.setState({
            friends: this.state.friends.concat([friend])
        })
    },

    render: function () {
        return (
            <div>
                <h3> Name: {this.state.name} </h3>
                <AddFriend addNew={this.addFriend} />
                <ShowList names={this.state.friends}/>
            </div>
        )
    }
});

var AddFriend = React.createClass({
   getInitialState: function(){
       return{
           newFriend: ''
       }
   },

    propTypes: {
        addNew: React.PropTypes.func.isRequired
    },

    updateNewFriend: function(e){
        this.setState({
            newFriend: e.target.value
        });
    },

    handleAddNew: function(){
        this.props.addNew(this.state.newFriend);
        this.setState({
            newFriend:''
        })
    },

    render: function(){
        return (
            <div>
                <input type="text" value={this.state.newFriend} onChange={this.updateNewFriend} />
                <button onClick={this.handleAddNew}> Add Friend </button>
            </div>
        );
    }
});

var ShowList = React.createClass({
    render: function () {
        var listItems = this.props.names.map(function (friend) {
            return <li> {friend} </li>;
        });

        return (
            <div>
                <h3> Friends </h3>
                <ul>
                    {listItems}
                </ul>
            </div>
        )
    }
});

module.exports = FriendsContainer;