var React = require('react');
var Table = require('./Table.react.js');
var EntryForm = require('./EntryForm.react.js');


/**
 * data: array of entity data
 * formInUpdateMode: boolean if the builder is in update mode it behaves differently
 * entityDefault: This is needed for our form inputs which are Controlled Components https://facebook.github.io/react/docs/forms.html.
 * entityHistoric: holds the original data of the entity being updated
 */
var ListBuilder = React.createClass({
    getInitialState: function () {
        var entityDefault = this.returnEmptyEntity();
        var entityHistoric = this.returnEmptyEntity();
        var formInUpdateMode = false;
        var tableHeaderFields = [
            {col: 1, columnStyle: "col-xs-5", columnText: "First Name", sortHandle: "fName"},
            {col: 2, columnStyle: "col-xs-5", columnText: "Last Name", sortHandle: "lName"},
            {col: 3, columnStyle: "col-xs-1", columnText: null},
            {col: 4, columnStyle: "col-xs-1", columnText: null}
        ];

        var data = [
            {id: 1, fName: "Chris", lName: "Legge"},
            {id: 2, fName: "Peter", lName: "Hurt"},
            {id: 3, fName: "Josh", lName: "Nugent"},
            {id: 4, fName: "Jens", lName: "Koeplinger"},
            {id: 5, fName: "Scott", lName: "Mandelsohn"},
            {id: 6, fName: "Spencer", lName: "Poplin"},
            {id: 7, fName: "Gene", lName: "Spooner"},
            {id: 8, fName: "Dale", lName: "Borget"},
            {id: 9, fName: "Sue", lName: "Haselsberger"}
        ];
        data = this.addDisplayIdToEntityData(data);

        return {
            data: data,
            formInUpdateMode: formInUpdateMode,
            entityDefault: entityDefault,
            entityHistoric: entityHistoric,
            tableHeaderFields: tableHeaderFields
        }
    },

    sortEntityData: function (entityData) {
        entityData.sort(function (a, b) {
            var keyA = parseInt(a.displayOnlyId),
                keyB = parseInt(b.displayOnlyId);
            // Compare the 2 dates
            if (keyA < keyB) return -1;
            if (keyA > keyB) return 1;
            return 0;
        });
        return entityData
    },

    removeNullsFromEntityData: function (entityData) {
        entityData = entityData.filter(function (val) {
            return !(val === "" || typeof val == "undefined" || val === null);
        });
        return entityData
    },

    addDisplayIdToEntityData: function (entityData) {
        $(entityData).each(function (i, o) {
            $(this).attr('displayOnlyId', i)
        });
        return entityData
    },

    /**
     * takes our entityDefault and nulls it out.
     */
    clearEntityDefault: function () {
        var emptyEntity = this.returnEmptyEntity();
        this.setState({
            entityDefault: emptyEntity
        })
    },

    /**
     * returns an entity with null values
     */
    returnEmptyEntity: function () {
        var emptyEntity = {id: null, displayOnlyId: null, fName: "", lName: ""};
        return {
            emptyEntity
        }
    },

    /**
     * returns a boolean opposite to the current.  called when the form switches modes
     */
    updateformInUpdateMode: function () {
        var bool = !this.state.formInUpdateMode;
        this.setState({
            formInUpdateMode: bool
        })
    },

    /**
     * all inputs in react should be controlled components, they derive there value from an object.
     * you can see it in action when you click edit and modify the text
     * @param e
     */
    handleFirstNameValueChange: function (e) {
        var tempEntity = this.state.entityDefault;
        tempEntity.fName = e.target.value;
        this.setState({
            entityDefault: tempEntity
        })
    },

    /**
     * all inputs in react should be controlled components, they derive there value from an object.
     * you can see it in action when you click edit and modify the text
     * @param e
     */
    handleLastnameValueChange: function (e) {
        var tempEntity = this.state.entityDefault;
        tempEntity.lName = e.target.value;
        this.setState({
            entityDefault: tempEntity
        })
    },

    /**
     * Populates our entityDefault with form data
     * @param id
     */
    setEntityDefault: function (id) {
        var result = $.grep(this.state.data, function (e) {
            return e.displayOnlyId == id;
        });
        this.setState({
            entityDefault: result[0],
            firstName: result[0].fName,
            lastName: result[0].lName
        })
    },

    /**
     * restores the data from entityHistorical to our data array
     */
    handleUndo: function () {
        var tempData = this.state.data;
        delete tempData[this.state.entityHistoric.displayOnlyId];
        tempData = this.removeNullsFromEntityData(tempData);
        console.log("entityHistoric: " + JSON.stringify(this.state.entityHistoric));
        tempData.push(this.state.entityHistoric);
        tempData = this.sortEntityData(tempData);
        this.setState({
            data: tempData
        })
    },

    /**
     * deletes entry from the array
     * @param id
     */
    deleteEntityFromDataArray: function (id) {

        var tempData = this.state.data;
        delete tempData[id];
        tempData = this.removeNullsFromEntityData(tempData);
        tempData = this.addDisplayIdToEntityData(tempData);
        this.setState({
            data: tempData
        })
    },

    /**
     * Adds a person to the array
     * @param _fName
     * @param _lName
     */
    addEntityToDataArray: function () {
        var tempData = this.state.data;
        var tempPerson = {id: 0, fName: this.state.entityDefault.fName, lName: this.state.entityDefault.lName};
        tempPerson.displayOnlyId = tempData.length;
        tempData.push(tempPerson);
        this.setState({
            data: tempData
        })
    },

    /**
     * sets our entityHistoric
     * @param entity
     */
    setEntityHistoric: function (entity) {
        console.log("setEntityHistoric: " + JSON.stringify(entity));
        this.setState({
            entityHistoric: entity
        })
    },


    getInputFields: function () {
        var inpuFields = [
            {
                divClassName: "col-xs-4",
                required: true,
                placeHolder: "First Name",
                inputType: "text",
                ref: "inputFName",
                onChangeHandler: this.handleFirstNameValueChange,
                inpValue: this.state.entityDefault.fName
            },
            {
                divClassName: "col-xs-4",
                required: true,
                placeHolder: "Last Name",
                inputType: "text",
                ref: "inputLName",
                onChangeHandler: this.handleLastnameValueChange,
                inpValue: this.state.entityDefault.lName
            }
        ];
        return inpuFields;
    },


    render: function () {
        return (
            <div>
                <EntryForm
                    addPerson={this.addEntityToDataArray}
                    updateformInUpdateMode={this.updateformInUpdateMode}
                    handleFirstNameValueChange={this.handleFirstNameValueChange}
                    handleLastnameValueChange={this.handleLastnameValueChange}
                    clearEntityDefault={this.clearEntityDefault}
                    setEntityDefault={this.setEntityDefault}
                    getInputFields={this.getInputFields}
                    handleUndo={this.handleUndo}
                    entityDefault={this.state.entityDefault}
                    entityHistoric={this.state.entityHistoric}
                    formInUpdateMode={this.state.formInUpdateMode}/>
                <Table
                    deleteEntityFromDataArray={this.deleteEntityFromDataArray}
                    setEntityDefault={this.setEntityDefault}
                    updateformInUpdateMode={this.updateformInUpdateMode}
                    setEntityHistoric={this.setEntityHistoric}
                    tableHeaderFields={this.state.tableHeaderFields}
                    entityDefault={this.state.entityDefault}
                    data={this.state.data}
                    formInUpdateMode={this.state.formInUpdateMode}/>
            </div>
        )
    }
});

module.exports = ListBuilder;