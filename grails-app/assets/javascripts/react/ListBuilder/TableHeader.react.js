var React = require('react');
var HeaderColumn = require('./HeaderColumn.react.js');

var TableHeader = React.createClass({


    render: function () {
        var sortData = this.props.sortData;
        var tableHeaderColumns = this.props.tableHeaderFields.map(function (entity) {
            return <HeaderColumn key={entity.col}  sortData={sortData} columnClassName={entity.columnStyle} columnText={entity.columnText} sortHandle={entity.sortHandle}/>
        });

        return (
            <thead>
            <tr>
                {tableHeaderColumns}
            </tr>
            </thead>
        )
    }
});

module.exports = TableHeader;