var React = require('react');
var TableRow = require('./TableRow.react.js');
var TableHeader = require('./TableHeader.react.js');

var Table = React.createClass({
    getInitialState: function () {
        return {
            sortDir: "asc",
            sortColumn: "fName"
        };
    },

    sortAsc: function(sortHandle){
        return function(a,b){
            if( a[sortHandle] > b[sortHandle]){
                return 1;
                return -1;
            }
            return 0;
        }
    },

    sortDesc: function(sortHandle){
        return function(a,b){
            if( a[sortHandle] < b[sortHandle]){
                return 1;
                return -1;
            }
            return 0;
        }
    },

    sortData: function(sortDirection, sortHandle){
        this.setState({
            sortDir: sortDirection,
            sortColumn: sortHandle,
        });
        console.log(sortDirection + " -- " +sortHandle);
    },

    renderSort: function () {
        if(this.state.sortDir === "asc"){
            this.props.data.sort(this.sortAsc(this.state.sortColumn));
        } else{
            this.props.data.sort(this.sortDesc(this.state.sortColumn));
        }
    },

    render: function () {
        this.renderSort();
        var updateformInUpdateMode = this.props.updateformInUpdateMode;
        var setEntityDefault = this.props.setEntityDefault;
        var setEntityHistoric = this.props.setEntityHistoric;
        var formInUpdateMode = this.props.formInUpdateMode;
        var entityDefault = this.props.entityDefault;
        var deleteEntityFromDataArray = this.props.deleteEntityFromDataArray;
        var tableHeader = <TableHeader sortData={this.sortData} tableHeaderFields={this.props.tableHeaderFields}/>
        var rows = this.props.data.map(function (rowEntity) {
            return <TableRow
                deleteEntityFromDataArray={deleteEntityFromDataArray}
                formInUpdateMode={formInUpdateMode}
                updateformInUpdateMode={updateformInUpdateMode}
                setEntityDefault={setEntityDefault}
                entityDefault={entityDefault}
                setEntityHistoric={setEntityHistoric}
                entity={rowEntity}
                key={rowEntity.displayOnlyId}/>
        });
        return (
            <div className="row">
                <table className="table table-striped table-bordered">
                    {tableHeader}
                    <tbody>{rows}</tbody>
                </table>
            </div>
        )
    }
});

module.exports = Table;