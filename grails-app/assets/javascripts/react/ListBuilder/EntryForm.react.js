var React = require('react');
var DivInput = require('./DivInput.react.js');

var EntryForm = React.createClass({

    getInitialState: function () {
        var formFieldData = this.props.getInputFields();
        return {
            formFieldData: formFieldData
        }
    },

    componentWillReceiveProps: function (nextProps) {
        this.setState({
            formFieldData: nextProps.getInputFields()
        });
    },

    onAddUpdateButtonClick: function () {
        if (!this.props.formInUpdateMode) {
            this.props.addPerson();
        }
        this.clearFields();

    },

    clearFields: function () {
        if (this.props.formInUpdateMode) {
            this.props.updateformInUpdateMode();
        }
        this.props.clearEntityDefault();
    },

    areButtonsDisabled: function () {
        var disabled = false;
        $.each(this.state.formFieldData, function (index, item) {
            if (item.required) {
                if (item.inpValue == "" || item.inpValue == undefined) {
                    disabled = true;
                }
            }
        });
        return disabled;
    },

    onClearUndorButtonClick: function () {
        if (this.props.formInUpdateMode) {
            this.props.handleUndo();
            this.props.updateformInUpdateMode();
        }
        this.clearFields();
    },

    getAddUpdateButtonText: function () {
        var buttonText = "Add";
        if (this.props.formInUpdateMode == true) {
            buttonText = "Update"
        }
        return buttonText;
    },

    getClearUndorButtonText: function () {
        var buttonText = "Clear";
        if (this.props.formInUpdateMode == true) {
            buttonText = "Undo"
        }
        return buttonText;
    },

    render: function () {
        var counter = 0;
        var formFields = this.state.formFieldData.map(function (inpFiled) {
            counter = counter + 1;
            return <DivInput
                key={counter}
                required={inpFiled.required}
                divClassName={inpFiled.divClassName}
                inputType={inpFiled.inputType}
                placeHolder={inpFiled.placeHolder}
                ref={inpFiled.ref}
                onChangeHandler={inpFiled.onChangeHandler}
                value={inpFiled.inpValue}/>
        });

        return (
            <div className="form-horizontal listBuilderForm">
                <div className="form-group">
                    {formFields}
                    <div className="col-xs-2">
                        <button className="btn btn-success btn-block" onClick={this.onAddUpdateButtonClick}
                                disabled={this.areButtonsDisabled()}>{this.getAddUpdateButtonText()}</button>
                    </div>
                    <div className="col-xs-2">
                        <button className="btn btn-danger btn-block" onClick={this.onClearUndorButtonClick}
                                disabled={this.areButtonsDisabled()}>{this.getClearUndorButtonText()}</button>
                    </div>
                </div>
            </div>
        )
    }
});

module.exports = EntryForm;