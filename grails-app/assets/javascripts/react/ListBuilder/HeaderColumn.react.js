var React = require('react');

var HeaderColumn = React.createClass({
    sortColumnAsc: function(){
        this.props.sortData("asc", this.props.sortHandle);
    },

    sortColumnDesc: function(){
        this.props.sortData("desc", this.props.sortHandle);
    },


    render: function(){
        console.log("text: " +this.props.columnText)
        if(this.props.columnText == null){
            return(
                <th className={this.props.columnClassName} onClick={this.sortColumn}>{this.props.columnText}</th>
            )
        } else{
            return(
                <th className={this.props.columnClassName}>{this.props.columnText}
                    &nbsp; <span className="glyphicon glyphicon-menu-down" aria-hidden="true" onClick={this.sortColumnDesc}></span>
                    &nbsp; <span className="glyphicon glyphicon-menu-up" aria-hidden="true" onClick={this.sortColumnAsc}></span>
                </th>
            )
        }

    }
});

module.exports = HeaderColumn;