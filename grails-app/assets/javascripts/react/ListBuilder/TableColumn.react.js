var React = require('react');

var TableColumn = React.createClass({
    render: function(){
        return(
            <td className={this.props.columnClassName}>{this.props.columnText}</td>
        )
    }
});

module.exports = TableColumn;