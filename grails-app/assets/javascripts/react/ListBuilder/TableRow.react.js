var React = require('react');
var TableColumn = require('./TableColumn.react.js');

var TableRow = React.createClass({
    onEditClick: function () {
        this.props.setEntityHistoric(JSON.parse(JSON.stringify(this.props.entity))); // want to clone it, not a poiter to obj
        this.props.setEntityDefault(this.props.entity.displayOnlyId);
        this.props.updateformInUpdateMode();
    },

    onDeleteClick: function () {
        this.props.deleteEntityFromDataArray(this.props.entity.displayOnlyId);
    },

    isEditButtonDisabled: function () {
        var disabled = false;
        if (this.props.formInUpdateMode) {
            disabled = true;
        }
        return disabled;
    },


    render: function () {
        var highlightClass = null;
        var tableColumns = [];
        $.each(this.props.entity, function(key, value) {
            if (key !== ("id") && key !== ("displayOnlyId")){
                tableColumns.push(<TableColumn key={key} columnClassName ="listBuilderForm-td-topPad" columnText = {value} />);
            }
        });

        if (this.props.entity.displayOnlyId === this.props.entityDefault.displayOnlyId) {
            highlightClass = 'highlightBorder';
        }
        return (
            <tr className={highlightClass}>
                {tableColumns}
                <td>
                    <button onClick={this.onEditClick} disabled={this.isEditButtonDisabled()}
                            className="btn btn-warning btn-block">Edit
                    </button>
                </td>
                <td>
                    <button onClick={this.onDeleteClick} disabled={this.isEditButtonDisabled()}
                            className="btn btn-danger btn-block">Delete
                    </button>
                    <input type="hidden" name="displayOnlyId" value={this.props.entity.displayOnlyId}/>
                </td>
            </tr>
        )
    }
});

module.exports = TableRow;