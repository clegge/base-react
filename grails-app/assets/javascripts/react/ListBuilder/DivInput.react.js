var React = require('react');

var DivInput = React.createClass({
    render: function () {
        if (this.props.required) {
            return (
                <div className={this.props.divClassName}>
                    <input type={this.props.inputType} placeholder={this.props.placeHolder} className="form-control"
                           ref={this.props.ref} onChange={this.props.onChangeHandler} value={this.props.value}
                           required/>
                </div>
            )
        } else {
            return (
                <div className={this.props.divClassName}>
                    <input type={this.props.inputType} placeholder={this.props.placeHolder} className="form-control"
                           ref={this.props.ref} onChange={this.props.onChangeHandler} value={this.props.value}/>
                </div>
            )
        }
    }
});

module.exports = DivInput;