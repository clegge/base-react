var React = require('react');
var ReactDOM = require('react-dom');
var ListBuilder = require('./ListBuilder/ListBuilder.react.js');
var InputOnChange = require('./Tutorial/InputOnChange.react.js');
var FriendsContainer = require('./Tutorial/FriendsContainer.react.js');


ReactDOM.render(
    <ListBuilder />,
    document.getElementById('react-list-builder')
);

/*
ReactDOM.render(
    <InputOnChange />,
    document.getElementById('tutorial')
);

ReactDOM.render(
    <FriendsContainer />,
    document.getElementById('friendsContainer')
);
*/
